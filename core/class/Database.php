<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Database
 *
 * @author arif
 */
class Database {

    //put your code here

    public function init() {
        global $autoload;
        global $db;
        $db_conn = null;
        if (isset($autoload->connection) && !empty($autoload->connection)) {
            if (in_array("db", $autoload->connection)) {
                if (isset($db->mysqli) && !empty($db->mysqli)) {
                    $servername = $db->mysqli['hostname'];
                    $username = $db->mysqli['username'];
                    $password = $db->mysqli['password'];
                    $dbname = $db->mysqli['database'];
                    try {
                        $db_conn = mysqli_connect($servername, $username, $password, $dbname);
                        // Check connection
                        if (mysqli_connect_errno()) {
                            force_stop("Failed to connect to MySQL: " . mysqli_connect_error());
                        }
                    } catch (PDOException $e) {
                        force_stop("Connection failed: " . $e->getMessage());
                    }
                } elseif (isset($db->pdo) && !empty($db->pdo)) {
                    $servername = $db->pdo['hostname'];
                    $username = $db->pdo['username'];
                    $password = $db->pdo['password'];
                    $dbname = $db->pdo['database'];
                    try {
                        $db_conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                        // set the PDO error mode to exception
                        $db_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        return true;
                    } catch (PDOException $e) {
                        force_stop("Connection failed: " . $e->getMessage());
                    }
                }
            }
        }
        return $db_conn;
    }

    public function close($conn = null) {
        $conn->close();
    }

}
