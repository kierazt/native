<?php

namespace CoreBootstrap;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Bootstrap
 *
 * @author arif
 */
class Bootstrap {

    //put your code here
    var $global_variable;

    public function init() {
        global $config;
        global $route;
        $global_variables = (object) array(
                    'base_url' => HTTP . REQUEST_URI,
                    'static_url' => HTTP . REQUEST_URI,
                    'config' => $config,
                    'routes' => (object) array(
                        'module' => $route->default_module,
                        'controller' => $route->default_controller,
                        'method' => 'index'
                    )
        );
        return $global_variables;
    }

    public function _get_module() {
        $init = \CoreBootstrap\Bootstrap::init();
        return $init->routes->module;
    }

    public function _get_controller() {
        $init = \CoreBootstrap\Bootstrap::init();
        return $init->routes->controller;
    }

    public function _get_method() {
        $init = \CoreBootstrap\Bootstrap::init();
        return $init->routes->method;
    }


}
