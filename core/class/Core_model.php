<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Core_model
 *
 * @author arif
 */
class Core_model {

    //put your code here

    public function query($sql = '') {
        return Active_record::query($sql);
    }

    public function find($keyword = 'all', $options = array()) {
        return Active_record::find($keyword, $options);
    }

}
