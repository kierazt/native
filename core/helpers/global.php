<?php

if (!function_exists('force_stop')) {

    function force_stop($text) {
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $start = $time;

        $trace = debug_backtrace();
        echo "<pre><strong>This is force stop!!!</strong><br/><hr/>";
        var_dump($text);

        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $finish = $time;
        $total_time = round(($finish - $start), 4);
        echo '<br/>Page generated in ' . $total_time . ' seconds.';
        die;
    }

}


if (!function_exists('debug')) {

    function debug($text) {
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $start = $time;

        $trace = debug_backtrace();
        echo "<pre><strong>file: " . $trace[0]['file'] . ", line: " . $trace[0]['line'] . "</strong><br/><hr/>";
        var_dump($text);

        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $finish = $time;
        $total_time = round(($finish - $start), 4);
        echo '<br/>Page generated in ' . $total_time . ' seconds.';
        die;
    }

}

if (!function_exists('get_ip')) {

    function get_ip() {
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_REAL_IP', 'REMOTE_ADDR', 'HTTP_FORWARDED_FOR', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED') as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {
                        return $ip;
                    }
                }
            }
        }
    }

}


if (!function_exists('re_assign_array')) {

    function re_assign_array($array = null, $find = '', $type = '', $delimeter = '/') {
        if ($array != null) {
            $_1 = explode($delimeter, $array);
            $new_array = array();
            foreach ($_1 AS $key => $val) {
                $new_array[] = ucfirst($val);
                if ($val == strtolower($find)) {
                    $new_array[] = ucfirst($type);
                }
            }
            $result = $new_array;
        }
        return $result;
    }

}

if (!function_exists('convert_result_to')) {

    function convert_result_to($data = null, $type = 'path') {
        $result = '';
        if ($data != null) {
            if ($type == 'path') {
                $result = implode('/', $data);
            } elseif ($type == 'array' && is_object($data)) {
                $result = array();
                foreach ($data AS $key => $val) {
                    $result[] = $val;
                }
            } elseif ($type == 'json' && is_array($data)) {
                foreach ($data AS $key => $val) {
                    $result[] = (object) $val;
                }
            }
        }
        return $result;
    }

}

if (!function_exists('get_segment')) {

    function get_segment($str = null, $delimeter = '/') {
        if ($str != null) {
            $res = explode($delimeter, $str);
            $segment = array();
            for ($i = 0, $j = 1; $i < count($res); $i++, $j++) {
                if ($j < count($res)) {
                    $segment['segment_' . $j] = $res[$j];
                }
            }
            return $segment;
        }
    }

}

