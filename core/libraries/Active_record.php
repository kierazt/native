<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Active_record
 *
 * @author arif
 */
class Active_record {

    //put your code here

    public function query($query = array(), $keyword = 'select') {
        if ($query) {
            if ($result = Database::init()->query($query)) {
                // Fetch one and one row
                if ($result) {
                    if ($keyword == 'select') {
                        $res_arr = array();
                        while ($row = mysqli_fetch_assoc($result)) {
                            $res_arr[] = (object) $row;
                        }
                        return $res_arr;
                    } else {
                        return true;
                    }
                } else {
                    force_stop("Error: " . $query . "<br>" . Database::init()->error);
                }
                Database::close($this->conn_open);
            }
        }
    }

    public function find($keyword = 'all', $options = array()) {
        $table = '';
        if (isset($options['table']) && !empty($options['table'])) {
            $table = strtolower($options['table']) . ' a ';
        }
        $fields = '*';
        if (isset($options['fields']) && !empty($options['fields'])) {
            $field = '';
            if (is_array($options['fields'])) {
                foreach ($options['fields'] AS $key => $val) {
                    if (!empty($field))
                        $field .= ', ';
                    $field .= "'" . $val . "'";
                }
            }
            $fields = $field;
        }
        $joins = '';
        if (isset($options['joins']) && !empty($options['joins'])) {
            $arr_joins = '';
            foreach ($options['joins'] AS $key => $val) {
                if (isset($val['type']) && !empty($val['type'])) {
                    $join_type = strtoupper($val['type']);
                } else {
                    $join_type = 'LEFT';
                }
                $table_join = strtolower(trim($val['table']));
                $condition_join = strtolower($val['on']);
                $arr_joins .= $join_type . ' JOIN ' . $table_join . ' ON ' . $condition_join;
            }
            $joins = $arr_joins;
        }
        $where = '';
        if (isset($options['conditions']) && !empty($options['conditions'])) {
            $conditions = '';
            $cond_new = array_keys($options['conditions']);
            for ($j = 0; $j < count($cond_new); $j++) {
                $cond_new2 = array_keys($options['conditions'][$cond_new[$j]]);
                $param = $cond_new2[0];
                $key = $options['conditions'][$cond_new[$j]][$cond_new2[0]];
                if (is_string($key)) {
                    $key = "'" . $key . "'";
                }
                if ($cond_new[$j] == 'where') {
                    $conditions .= ' WHERE ' . $param . ' = ' . $key;
                } elseif ($cond_new[$j] == 'and') {
                    $conditions .= ' AND ' . $param . ' = ' . $key;
                } elseif ($cond_new[$j] == 'or') {
                    $conditions .= ' OR ' . $param . ' = ' . $key;
                }
            }
            $where = $conditions;
        }

        $like = '';
        if (isset($options['like']) && !empty($options['like'])) {
            $search_field = $options['like'][0];
            $search_value = $options['like'][1];
            $search_type = $options['like'][2];
            switch ($search_type) {
                case 'F' :
                    $type = "'" . $search_value . "%'";
                    break;
                case 'L':
                    $type = "'%" . $search_value . "'";
                    break;
                case 'O':
                    $type = "'%" . $search_value . "%'";
                    break;
                //case 'S':
                //    $type = "'_" . $search_value . "'" . '% ';
                //    break;
                //case 'P':
                //    $type = $search_value . '% ';
                //    break;
                //case 'B':
                //    $type = $search_value . '% ';
                //    break;
            }
            //type : first F, last L, or O, second S, Prefixs P, Both Prefixs B
            $like = ' WHERE ' . $search_field . ' LIKE ' . $type;
        }
        $group = '';
        if (isset($options['group']) && !empty($options['group'])) {
            $group = ' GROUP BY ' . $options['group'];
        }

        $order = '';
        if (isset($options['order']) && !empty($options['order'])) {
            $arr_order = '';
            foreach ($options['order']['key'] AS $key => $value) {
                if (!empty($arr_order))
                    $arr_order .= ', ';
                $arr_order .= $value;
            }
            $order = ' ORDER BY ' . $arr_order . ' ' . $options['order']['type'];
        }
        $sql = 'SELECT ' . $fields . ' FROM ' . $table . $joins . $where . $like . $group . $order;
        $result = Active_record::query($sql);
        if (isset($result) && !empty($result)) {
            if ($keyword == 'all') {
                return $result;
            } elseif ($keyword == 'first') {
                return $result[0];
            }
        } else {
            return null;
        }
    }

}
