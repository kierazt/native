<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include_once ROOT_PATH . DS . 'core' . DS . 'helpers' . DS . 'global.php';
include_once APP_PATH . DS . 'config' . DS . 'autoload.conf.php';
include_once APP_PATH . DS . 'config' . DS . 'config.conf.php';
include_once APP_PATH . DS . 'config' . DS . 'routes.conf.php';
include_once APP_PATH . DS . 'config' . DS . 'db.conf.php';
include_once ROOT_PATH . DS . 'core' . DS . 'class' . DS . 'Bootstrap.php';
include_once ROOT_PATH . DS . 'core' . DS . 'class' . DS . 'Database.php';
include_once ROOT_PATH . DS . 'core' . DS . 'libraries' . DS . 'Active_record.php';

$init = \CoreBootstrap\Bootstrap::init();
$_get_module = \CoreBootstrap\Bootstrap::_get_module();
$_get_controller = \CoreBootstrap\Bootstrap::_get_controller();
$_get_method = \CoreBootstrap\Bootstrap::_get_method();

//load core_controller and core_model
include_once ROOT_PATH . DS . 'core' . DS . 'class' . DS . 'Core_controller.php';
include_once ROOT_PATH . DS . 'core' . DS . 'class' . DS . 'Core_model.php';

//load app_controller and app_model
include_once APP_PATH . DS . 'core' . DS . 'App_Controller.php';
include_once APP_PATH . DS . 'core' . DS . 'App_Model.php';

if (REQUEST_URI == '/') {
    include_once APP_PATH . DS . 'modules' . DS . $_get_module . DS . 'controllers' . DS . $_get_controller . '.php';
    $_get_controller::$_get_method();
}
