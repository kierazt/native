<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author arif
 */
class User extends App_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $options = array(
            'table' => 'tbl_users',
//            'conditions' => array(
//                'where' => array(
//                    'a.is_active' => 1
//                ),
//                'and' => array(
//                    'a.id' => 1
//                )
//            ),
            'fields' => array(
                'a.id', 'a.username'
            ),
            'like' => array(
                'a.username', 'arif', 'F'
            ),
            'order' => array(
                'key' => array(
                    'a.username',
                    'a.id'
                ),
                'type' => 'ASC'
            ),
            'group' => 'a.id',
            'joins' => array(
                array(
                    'table' => 'tbl_user_groups b',
                    'on' => 'a.id = b.user_id',
                    'type' => 'left'
                )
            )
        );
        //$sql = 'SELECT * FROM tbl_users';
        debug(App_Model::find('all', $options));
    }

}
