<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$route = (object) array(
    'default_module' => 'frontend',
    'default_controller' => 'user',
    '/' => 'frontend/user',
    'home' => 'frontend/user',
    'login' => 'frontend/user/login',
    'logout' => 'frontend/user/logout',
    'dashboard' => 'frontend/user/dashboard',
    'backend/login' => 'backend/user/login',
    'backend/logout' => 'backend/user/logout',
    'backend/dashboard' => 'backend/user/dashboard'
);
