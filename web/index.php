<?php

define('DS', DIRECTORY_SEPARATOR);
//this is app mvc will be places
$AppPath = 'apps';

//this is core system
$CorePath = 'core' . DS . 'class';

//initialize system global variables
define('ROOT_PATH', dirname(dirname(__FILE__)));
define('APP_PATH', ROOT_PATH . DS . $AppPath);
define('CORE_PATH', ROOT_PATH . DS . $CorePath);
define('REQUEST_URI', $_SERVER['REQUEST_URI']);
define('HTTP', 'http://');
define('HTTPS', 'https://');



include_once ROOT_PATH . DS . 'core' . DS . 'config' . DS . 'autoload.php';
